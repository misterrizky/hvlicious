<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        return view('page.home');
    }
    public function about()
    {
        return view('page.about');
    }
    public function work()
    {
        return view('page.work');
    }
}
