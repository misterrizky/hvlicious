<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController;

Route::group(['domain' => 'https://hvlicious.dev'], function() {
    Route::name('web.')->group(function(){
        Route::get('', [WebController::class, 'index'])->name('home');
        Route::get('about', [WebController::class, 'about'])->name('about');
        Route::get('works', [WebController::class, 'work'])->name('work');
    });
});