<script src="{{asset('semicolon/js/jquery.js')}}"></script>
<script src="{{asset('semicolon/js/plugins.min.js')}}"></script>

<!-- Footer Scripts ============================================= -->
<script src="{{asset('semicolon/js/functions.js')}}"></script>

<script>
    // Owl Carousel Scripts
    jQuery(window).on( 'pluginCarouselReady', function(){
        $('#oc-services').owlCarousel({
            items: 1,
            margin: 30,
            nav: false,
            dots: true,
            smartSpeed: 400,
            responsive:{
                576: { stagePadding: 30, items: 1 },
                768: { stagePadding: 30, items: 2 },
                991: { stagePadding: 150, items: 3 },
                1200: { stagePadding: 150, items: 3}
            },
        });
    });
</script>