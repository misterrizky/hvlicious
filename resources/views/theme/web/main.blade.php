<!DOCTYPE html>
<html dir="ltr" lang="en-US">
@include('theme.web.head')
<body class="stretched">
	<!-- Document Wrapper ============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Header ============================================= -->
		@include('theme.web.header')
        <!-- #header end -->
		<!-- Slider ============================================= -->
		@if(request()->is(''))
		@include('theme.web.slider')
		@endif
        <!-- #slider end -->
		<!-- Content ============================================= -->
		{{$slot}}
        <!-- #content end -->
		<!-- Footer ============================================= -->
		@include('theme.web.footer')
        <!-- #footer end -->
	</div>
    <!-- #wrapper end -->
	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-double-angle-up bg-white text-dark rounded-circle shadow"></div>
	<!-- External JavaScripts ============================================= -->
	@include('theme.web.js')
</body>
</html>